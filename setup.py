'''Install the crc_templates package'''

from setuptools import setup

setup(
    name='crc_templates',
    version='0.2',
    description='CRC common templates',
    author='Craig Ciccone',
    author_email='crc888@gmail.com',
    url='https://www.crc-web.cf/',
    packages=['crc_templates'],
    package_data={'crc_templates': ['templates/*']},
    install_requires=['Flask-Moment==0.5.1']
)
